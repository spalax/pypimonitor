// Reveal images of "takeitslow" class, one at a time.
// Used not to get banned by CDN because we request tens of images at once.
var elements = document.getElementsByClassName("takeitslow");

function reveal(i) {
  elements[i].src = elements[i].dataset.url;
  if (i < elements.length-1) {
    setTimeout(function() {reveal(i+1);}, 500);
  }
}
reveal(0);
